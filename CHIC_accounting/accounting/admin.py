from django.contrib import admin
from .models import User, Group, UserGroupMapping, Operation, Repository, RepoPermissionMapping, Model, Machine, ModelMachinePermissionMapping

admin.site.register(User)
admin.site.register(Group)
admin.site.register(UserGroupMapping)

admin.site.register(Operation)
admin.site.register(Repository)
admin.site.register(RepoPermissionMapping)

admin.site.register(Model)
admin.site.register(Machine)
admin.site.register(ModelMachinePermissionMapping)
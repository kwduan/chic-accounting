__author__ = 'me1kd'

from . import models
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group


class RepoPermissionMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RepoPermissionMapping


class UserGroupMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserGroupMapping
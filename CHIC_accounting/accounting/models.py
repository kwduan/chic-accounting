from django.db import models


class User(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=100, blank=False)
    authority_name = models.CharField(max_length=100, blank=False, default='')
    account_type = models.CharField(max_length=100, blank=False, default='')

    def __str__(self):
        return str(self.id)


class Group(models.Model):
    id = models.AutoField(primary_key=True)
    group_name = models.CharField(max_length=100, blank=False)
    institute_name = models.CharField(max_length=100, blank=False)
    authority_name = models.CharField(max_length=100, blank=False, default='')
    account_type = models.CharField(max_length=100, blank=False, default='')

    def __str__(self):
        return str(self.group_name)


class UserGroupMapping(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User)
    group_id = models.ForeignKey(Group)

    def __str__(self):
        return str(self.id)


class Operation(models.Model):
    id = models.AutoField(primary_key=True)
    operation_name = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return str(self.id)


class Repository(models.Model):
    id = models.AutoField(primary_key=True)
    repository_url = models.CharField(max_length=200, blank=False)
    repository_name = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return str(self.id)


class RepoPermissionMapping(models.Model):
    id = models.AutoField(primary_key=True)
    group_id = models.ForeignKey(Group)
    operation_id = models.ForeignKey(Operation)
    repository_id = models.ForeignKey(Repository)

    def __str__(self):
        return str(self.id)


class Model(models.Model):
    id = models.AutoField(primary_key=True)
    universal_id = models.CharField(max_length=100, blank=False)
    name = models.CharField(max_length=100, blank=False)
    resource_uri = models.CharField(max_length=200, blank=False)

    def __str__(self):
        return str(self.id)


class Machine(models.Model):
    id = models.AutoField(primary_key=True)
    machine_address = models.CharField(max_length=50, blank=False)
    name = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return str(self.id)


class ModelMachinePermissionMapping(models.Model):
    id = models.AutoField(primary_key=True)
    group_id = models.ForeignKey(Group)
    machine_id = models.ForeignKey(Machine)
    model_id = models.ForeignKey(Model)

    def __str__(self):
        return str(self.id)
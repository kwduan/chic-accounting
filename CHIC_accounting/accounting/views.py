from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . import utils
import os, json
from . import serializer
from . import models


accounting_root = os.path.dirname(os.path.realpath(__file__))
schema_root = accounting_root + '/JSON_schema/'


class Permission(APIView):
    def get(self, request):
        json_request = request.body
        # print(json_request)
        try:
            utils.validate_json_by_schema(json_request, schema_root + 'user_schema.json')
        except Exception as e:
            Response(e.message, status=status.HTTP_400_BAD_REQUEST)
        object_request = json.loads(json_request)
        if models.Group.object.filter(authority_name=object_request.authority,
                                      account_type=object_request.accout_type).count() == 0:
            # no appropriate group can be found with this user information
            return Response('No appropriate permission for this user.', status=status.HTTP_400_BAD_REQUEST)
        else:
            if models.User.object.filter(username=object_request.username,
                                         authority_name=object_request.authority,
                                         account_type=object_request.accout_type).count() == 0:
                # for each unique authority_name and accout_type pair, there is a default group in the system
                group = models.Group.object.get(group_name='default', authority_name=object_request.authority,
                                                account_type=object_request.accout_type)
                if models.RepoPermissionMapping.object.filter(group_id=group).count() == 0:
                    return Response('No appropriate repository permission for this user.',
                                    status=status.HTTP_400_BAD_REQUEST)
                else:
                    rpmappings = models.RepoPermissionMapping.object.filter(group_id=group)
                    # print(repo_id)
                    # todo: get the permission and serialize it
                    # add the user to local database for record purpose
                    # todo: add the user and the corresponding mapping found in previous step.
            else:
                # find the mapped group of the user
                user = models.User.object.get(username=object_request.username,
                                              authority_name=object_request.authority,
                                              account_type=object_request.accout_type)
                ugmapping = models.UserGroupMapping.object.get(user_id=user)
                group_id = ugmapping.group_id
                group = models.Group.object.get(id=group_id)
                rpmappings = models.RepoPermissionMapping.object.filter(group_id=group)
                # todo: get the permission and serialize it





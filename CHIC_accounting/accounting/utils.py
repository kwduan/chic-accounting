##
# @namespace utils
# This is a utility package used for unittest functions.

__author__ = 'me1kd'

import json
import jsonschema


## Get the result printed if a json validated with a json schema.
#  @param r_text the json text as string
#  @param schema_path the absolute path of the schema as string
def validate_json_by_schema(r_text, schema_path):
    r_data = json.loads(r_text)
    schema_file = open(schema_path, "r")
    schema_data = schema_file.read()
    schema_json = json.loads(schema_data)
    try:
        jsonschema.Draft4Validator(schema_json).validate(r_data)
    except jsonschema.ValidationError as e:
        print "json validation failed"
        raise
    except jsonschema.SchemaError as e:
        print "schema is invalid"
        raise
